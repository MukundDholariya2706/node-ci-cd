const express = require('express');
const http = require('http');
const app = express();
const server = http.createServer(app);

app.get('/about', (req, res) => {
    res.send({message: 'about'}).status(200);
})

app.get('/', (req, res) => {
    res.send({message: 'Hello'}).status(200);
})


server.listen(3000,() => {
    console.log('server is running on 3000 port')
})